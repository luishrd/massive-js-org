---
layout: Home
title: Home
description: 'A data access toolkit and ORM replacement for Node.js and PostgreSQL'
---

[![npm](https://img.shields.io/npm/v/massive.svg?label=massive&style=popout)](https://npmjs.org/package/massive)
![node](https://img.shields.io/node/v/massive.svg)
[![Build Status](https://img.shields.io/gitlab/pipeline/dmfay/massive-js.svg)](https://gitlab.com/dmfay/massive-js/pipelines)
[![Coverage Status](https://coveralls.io/repos/gitlab/dmfay/massive-js/badge.svg)](https://coveralls.io/gitlab/dmfay/massive-js)
[![npm](https://img.shields.io/npm/dw/massive.svg)](https://npmjs.org/package/massive)

Massive is a data mapper for Node.js that goes all in on PostgreSQL and embraces the power and flexibility of SQL and the relational model. With minimal abstractions for the interfaces and tools you already use, its goal is to do just enough to make working with your data and your database as easy and intuitive as possible, then get out of your way.

Massive is _not_ an object-relational mapper (ORM)! It doesn't use models, it doesn't track state, and it doesn't box you into working and thinking in terms of individual entities, barely scratching the surface of what PostgreSQL can do. Instead, Massive analyzes and builds an API for the data model expressed in your database's tables, views, and functions, simplifying routine SQL generation without hiding SQL itself so your applications can use Postgres to the fullest possible extent.

Here are some of the highlights:

* **Dynamic query generation**: Simple [criteria objects](https://massivejs.org/docs/criteria-objects) represent a wide array of comparison operations and even predicates delving into JSON fields. Outside the `WHERE` clause, Massive can generate everything from complex sorting with [`order`](https://massivejs.org/docs/options-objects#ordering-results) to true upserts with [`onConflictUpdate`](http://massivejs.org/docs/options-objects#onconflictupdate).
* **Join what you need, when you need it**: Call [`join()`](https://massivejs.org/docs/joins-and-result-trees#readablejoin) on any table or view to bring the programmatic flexibility of Massive's API and SQL statement generation to multiple relations at once.
* **Document storage**: PostgreSQL's JSONB data type makes blending relational and document techniques practical. Massive makes it easy: create document tables at runtime, pass objects in, get objects out, with metadata managed for you every step of the way.
* **Transactions**: [`db.withTransaction()`](https://massivejs.org/docs/tasks-and-transactions) executes a callback with full Massive API support in a transaction scope, automatically rolling changes back if an exception occurs.
* **Low overhead**: An API built from your schema means direct access to your tables, views, and functions; raw SQL ad hoc or managed in a central scripts directory; super-simple bulk operations; and no model classes to maintain!
* **Postgres everything**: Many, if not most, relational data access tools are designed for compatibility across multiple relational database management systems. Massive is not. By committing to a single RDBMS, Massive gains support for array fields and operations, regular expression matching, foreign tables, materialized views, and more features found in PostgreSQL but not its competition.

## Install

```bash
npm install massive --save
```

For next steps after that, see [Get Started](/get-started).

## Contributing

Visit the [project page](https://gitlab.com/dmfay/massive) to report issues or check out the code!

## Licensing

Massive is released under the [BSD 3-Clause license](https://gitlab.com/dmfay/massive-js/blob/master/LICENSE). You may use it free of charge, including in commercial software products you release under your own warranty. You may not hold the project or its contributors liable for any reason or claim its contributors endorse whatever you're doing with it. If you redistribute Massive you must include the copyright and license. This is automatic assuming you're using a package manager like npm or yarn.

MassiveJS.org content is covered by the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0) license](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode). You may copy and modify it to your heart's content, but if you want to share or distribute your modifications you can't make money off it, and your derivative work must be released under the same terms.
