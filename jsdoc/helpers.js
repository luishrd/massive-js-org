'use strict';

const {_identifier, anchorName, isConstructor, isEvent, isExternal, isFunction, isPrivate, methodSig, option, parentName} = require('dmd/helpers/ddata');
const handlebars = require('handlebars');

exports._link = (input, options) => {
  if (typeof input !== 'string') return null

  var linked, matches, namepath
  var output = {}

  /*
  test input for
  1. A type expression containing a namepath, e.g. Array.<module:Something>
  2. a namepath referencing an `id`
  3. a namepath referencing a `longname`
  */
  if ((matches = input.match(/.*?<(.*?)>/))) {
    namepath = matches[1]
  } else {
    namepath = input
  }

  options.hash = { id: namepath }
  linked = _identifier(options)
  if (!linked) {
    options.hash = { longname: namepath }
    linked = _identifier(options)
  }
  if (!linked) {
    output = { name: input, url: null }
  } else {
    output.name = input.replace(namepath, linked.name)
    if (isExternal.call(linked)) {
      if (linked.description) {
        output.url = '#' + anchorName.call(linked, options)
      } else {
        if (linked.see && linked.see.length) {
          var firstLink = parseLink(linked.see[0])[0]
          output.url = firstLink ? firstLink.url : linked.see[0]
        } else {
          output.url = null
        }
      }
    } else {
      // override links to classes being extended to point them at
      // split files
			const anchor = anchorName.call(linked, options);

      output.url = '/api/' + anchorName.call(linked, options)
    }
  }
  return output
};

exports.link = (longname, options) => {
  return options.fn(exports._link(longname, options));
};
