import {
  start,
  loadConfig,
  loadSourceFiles,
  generatePages,
  savePages,
  createMarkdownRenderer,
  createTemplateRenderer,
  helpers
} from 'fledermaus';
import moment from 'moment';
import slug from 'remark-slug';
import visit from 'unist-util-visit';

const indexer = (page, idx) => {
  page.index = idx;

  return page;
};

start('Building site...');

const config = loadConfig('config');
const options = config.base;

const toBuild = loadSourceFiles(options.staticFolder, options.staticExts, {
  renderers: {
    md: createMarkdownRenderer({
      plugins: [slug]
    })
  }
});

const pages = generatePages(toBuild, config, helpers, {
  jsx: createTemplateRenderer({
    root: options.templatesFolder
  })
});

savePages(pages, options.publicFolder);
